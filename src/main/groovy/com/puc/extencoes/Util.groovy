package com.puc.extencoes

import grails.util.Holders

/**
 * Created by hyago on 11/09/18.
 */
class Util {
    /**
     * Dada uma URI, retorna o controller e a action dela (id caso contenha).
     * @param uri
     * @return
     */
    static Map getControllerAction(String uri) {
        if (uri == null || uri.isEmpty()) return null
        final String contextPath = Holders.grailsApplication.mainContext.servletContext.contextPath
        final String[] uriSplited = uri.split(contextPath)
        if (uriSplited.size() > 0) {
            final String[] uriRestSplited = uriSplited[-1].split("/")
            final String controller = uriRestSplited.size() > 0 ? uriRestSplited[1] : null
            final String action = uriRestSplited.size() > 1 ? uriRestSplited[2] : null
            final String idStrg = uriRestSplited.size() > 3 ? uriRestSplited[3] : null

            if (action?.contains('?')) action = action.split('\\?')[0]
            if (idStrg?.contains('?')) idStrg = idStrg.split('\\?')[0]
            Map link = [controller: controller, action: action]
            if (idStrg?.isLong()) link.put('id', idStrg.toLong())
            return link
        }

        return null
    }
}