package com.puc.extencoes
/**
 * Created by Hyago on 29/05/2018.
 */
trait NotFoundRedirection {
    void notFound() {
        flash.error = "${getNomeEntidade()} não encontrado(a)."
        redirect action: "index", method: "GET"
    }

    abstract String getNomeEntidade()
}