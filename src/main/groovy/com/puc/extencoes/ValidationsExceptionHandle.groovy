package com.puc.extencoes

import grails.validation.ValidationException
import org.grails.core.exceptions.InvalidPropertyException

/**
 * Created by Hyago on 29/05/2018.
 */
trait ValidationsExceptionHandle {
    def handleValidationException(ValidationException e) {
        String error = "<strong>Errors encontrados</strong>"
        error += e.errors.allErrors.collect { "<li>" + g.message([error: it]).toString() + ".</li>" }.join("")
        flash.error = error
        voltarParaTelaAnterior()
    }

    def handleInvalidPropertyException(InvalidPropertyException e) {
        flash.error = e.message
        voltarParaTelaAnterior()
    }

    def handleGenericException(Exception e) {
        e.printStackTrace()
        flash.error = "Houve um Erro Interno."
        voltarParaTelaAnterior()
    }

    void voltarParaTelaAnterior() {
        final String erroUri = request.getHeader('referer')
        final Map lastLink = Util.getControllerAction(erroUri)
        if (lastLink) redirect(lastLink + [params: params])
    }
}