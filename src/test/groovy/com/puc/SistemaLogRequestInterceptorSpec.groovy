package com.puc

import grails.testing.web.interceptor.InterceptorUnitTest
import spock.lang.Specification

class SistemaLogRequestInterceptorSpec extends Specification implements InterceptorUnitTest<SistemaLogRequestInterceptor> {

    def setup() {
    }

    def cleanup() {

    }

    void "Test sistemaLogRequest interceptor matching"() {
        when:"A request matches the interceptor"
            withRequest(controller:"sistemaLogRequest")

        then:"The interceptor does match"
            interceptor.doesMatch()
    }
}
