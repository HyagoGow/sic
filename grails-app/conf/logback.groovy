import ch.qos.logback.classic.AsyncAppender
import ch.qos.logback.classic.encoder.PatternLayoutEncoder
import ch.qos.logback.core.ConsoleAppender
import ch.qos.logback.core.rolling.RollingFileAppender
import ch.qos.logback.core.rolling.TimeBasedRollingPolicy
import ch.qos.logback.core.util.FileSize
import grails.util.Environment

final String LOG_NAME = "ssevento".toUpperCase() //TODO alterar
final String PATTERN = "[%d{yyyy-MM-dd HH:mm:ss.SSS}] ${LOG_NAME} %p - %m%n"
List<String> appenderList = ["CONSOLE"]

appender("CONSOLE", ConsoleAppender) {
    encoder(PatternLayoutEncoder) {
        pattern = PATTERN
    }
}

if (Environment.current.name.toLowerCase() in ["homologation", "production"]) {
    final String LOG_PATH = (System.properties['catalina.base'] ?: '.') + "/logs"

    appender("DAILLY", RollingFileAppender){
        file = "${LOG_PATH}/${LOG_NAME}.log"
        rollingPolicy(TimeBasedRollingPolicy) {
            fileNamePattern = "${LOG_PATH}/${LOG_NAME}.log.%d{yyyy-MM-dd}"
            maxHistory = 90
            totalSizeCap = FileSize.valueOf("1GB")
        }
        encoder(PatternLayoutEncoder) {
            pattern = PATTERN
        }
    }

    appender("ASYNC", AsyncAppender) {
        appenderRef("DAILLY")
    }
    appenderList.add("ASYNC")
}

logger("gestaolocadora", ALL, appenderList, false)
logger("com.acception", ALL, appenderList, false)
logger("groovyx.net.http", ALL, appenderList, false)

root(ERROR, appenderList)