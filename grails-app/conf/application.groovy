info {
    app {
        name = '@showInfo.app.name@'
        version = '@showInfo.app.version@'
        grailsVersion = '@showInfo.app.grailsVersion@'
    }
}
spring {
    main.'banner-mode' = "off"
    groovy.template.'check-template-location' = false
}
//Spring Actuator Endpoints are Disabled by Default
endpoints {
    enabled = false
    jmx.enabled = true
}

grails {
    profile = "web"
    codegen.defaultPackage = "com.puc.sic"
    spring.transactionManagement.proxies = false

    gorm.failOnError = true

    plugins {
        twitterbootstrap.fixtaglib = true
    }

    plugin.console.enabled = true

    mime {
        disable.accept.header.userAgents = ['Gecko', 'WebKit', 'Presto', 'Trident']
        types {
            all = '*/*'
            atom = "application/atom+xml"
            css = "text/css"
            csv = "text/csv"
            form = "application/x-www-form-urlencoded"
            html = ["text/html", "application/xhtml+xml"]
            js = "text/javascript"
            json = ["application/json", "text/json"]
            multipartForm = "multipart/form-data"
            pdf = "application/pdf"
            rss = "application/rss+xml"
            text = "text/plain"
            hal = ["application/hal+json", "application/hal+xml"]
            xml = ["text/xml", "application/xml"]
        }
    }

    urlmapping.cache.maxsize = 1000

    controllers.defaultScope = "singleton"

    databinding.dateFormats = ['dd/MM/yyyy', 'dd/MM/yy', 'yyyy-MM-dd HH:mm:ss.S', "yyyy-MM-dd'T'hh:mm:ss'Z'"]

    converters.encoding = "UTF-8"

    views {
        'default' {
            codec = "html"
        }
        gsp {
            encoding = "UTF-8"
            htmlcodec = "xml"
            codecs {
                expression = "html"
                scriptlets = "html"
                taglib = "none"
                staticparts = "none"
            }
        }
    }

    exceptionresolver.params.exclude = ['password', 'senha']
}

endpoints.jmx.'unique-names' = true
server.contextPath = '/sic'

//-- DATASOURCES
hibernate {
    cache.queries = false
    cache.use_second_level_cache = false
    cache.use_query_cache = false
    //    format_sql = true
}

dataSource {
    pooled = true
    jmxExport = true
    driverClassName = "org.postgresql.Driver"
    dbCreate = "create" // one of 'create', 'create-drop', 'update', 'validate', ''
}

environments {
    development {
        dataSource {
            username = "postgres"
            password = "postgres"
            url = "jdbc:postgresql://localhost/sic_development"
        }
    }

    homologation {
        grails.serverURL = 'http://35.198.16.201/sic'

        dataSource {
            username = "postgres"
            password = "postgres"
            url = "jdbc:postgresql://localhost:5432/sic_homologation"
        }
    }
}


// Added by the Spring Security Core plugin:
grails.plugin.springsecurity.userLookup.userDomainClassName = 'com.puc.seguranca.Usuario'
grails.plugin.springsecurity.userLookup.authorityJoinClassName = 'com.puc.seguranca.UsuarioAutoridade'
grails.plugin.springsecurity.authority.className = 'com.puc.seguranca.Autoridade'
grails.plugin.springsecurity.password.algorithm = 'bcrypt'
grails.plugin.springsecurity.successHandler.defaultTargetUrl = '/dashboard/redirecionarUsuario' //redireciona dependendo do acesso do usuário
grails.plugin.springsecurity.filterChain.chainMap = [
        [pattern: '/assets/**', filters: 'none'],
        [pattern: '/**/js/**', filters: 'none'],
        [pattern: '/**/css/**', filters: 'none'],
        [pattern: '/**/images/**', filters: 'none'],
        [pattern: '/**/favicon.ico', filters: 'none'],
        [pattern: '/**', filters: 'JOINED_FILTERS']
]

grails.plugin.springsecurity.controllerAnnotations.staticRules = [
        [pattern: '/', access: ['permitAll']],
        [pattern: '/error', access: ['permitAll']],
        [pattern: '/assets/**', access: ['permitAll']],
        [pattern: '/**/js/**', access: ['permitAll']],
        [pattern: '/**/css/**', access: ['permitAll']],
        [pattern: '/**/images/**', access: ['permitAll']],
        [pattern: '/**/favicon.ico', access: ['permitAll']],
        [pattern: '/console/**', access: ['ROLE_DESENVOLVEDOR']],
        [pattern: '/static/console/**', access: ['ROLE_DESENVOLVEDOR']],

        [pattern: '/dashboard/redirecionarUsuario', access: ['permitAll']],
        [pattern: '/dashboard/administrativo', access: ['ROLE_DESENVOLVEDOR', 'ROLE_ADMINISTRADOR', 'ROLE_VENDEDOR']],
        [pattern: '/dashboard/cliente', access: ['ROLE_DESENVOLVEDOR', 'ROLE_CLIENTE']],

        [pattern: '/pedido/**', access: ['ROLE_DESENVOLVEDOR', 'ROLE_ADMINISTRADOR', 'ROLE_VENDEDOR', 'ROLE_CLIENTE']],
        [pattern: '/produto/**', access: ['ROLE_DESENVOLVEDOR', 'ROLE_ADMINISTRADOR', 'ROLE_VENDEDOR']]
]