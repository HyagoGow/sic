<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="layout-administrativo"/>
    <title>Novo Produto</title>
</head>

<body>
<p>
    <g:link controller="produto" action="index" class="btn btn-primary">
        <i class="glyphicon glyphicon-list"></i> Produtos
    </g:link>
</p>

<alert:errorsIfExists/>

<div class="panel panel-primary">
    <div class="panel-heading">
        <h3 class="panel-title">Dados Básicos</h3>
    </div>
    <div class="panel-body">
        <g:form controller="produto" action="save" method="POST">
            <div class="row">
                <div class="form-group col-md-5">
                    <label for="nome">Nome *</label>
                    <input type="text" class="form-control" name="nome" id="nome" value="${produto?.nome}" required>
                </div>

                <div class="form-group col-md-5">
                    <label for="nome">Código *</label>
                    <input type="text" class="form-control" name="codigo" id="codigo" value="${produto?.codigo}" required>
                </div>

                <div class="form-group col-md-2">
                    <label for="nome">Preço *</label>
                    <div class="input-group">
                        <span class="input-group-addon">R$</span>
                        <input type="number" class="form-control" name="valor" id="valor" min="0" value="${produto?.valor}" required>
                    </div>
                </div>
            </div>

            <button type="submit" class="btn btn-primary">
                <i class="glyphicon glyphicon-save"></i> Salvar
            </button>
        </g:form>
    </div>
</div>
</body>
</html>