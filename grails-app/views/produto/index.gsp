<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="layout-administrativo"/>
    <title>Produtos</title>
</head>

<body>
<p>
    <g:link controller="produto" action="create" class="btn btn-primary">
        <i class="glyphicon glyphicon-plus"></i> Novo Produto
    </g:link>
</p>

<alert:errorsIfExists/>
<alert:successIfExists/>

<g:if test="${produtoCount > 0}">
    <table class="table table-bordered table-striped">
        <thead>
        <tr>
            <td>Código</td>
            <td>Nome</td>
            <td>Valor</td>
        </tr>
        </thead>
        <tbody>
        <g:each in="${produtoList}" var="produto">
            <tr>
                <td>${produto.codigo}</td>
                <td>${produto.nome}</td>
                <td><g:formatNumber number="${produto.valor}" type="currency"/></td>
            </tr>
        </g:each>
        </tbody>
    </table>
    <g:paginate total="${produtoCount ?: 0}"/>
</g:if>
<g:else>
    <div class="well text-center">
        <strong>SEM REGISTROS</strong>
    </div>
</g:else>
</body>
</html>