<li>
    <g:link controller="dashboard" action="redirecionarUsuario">
        <i class="fa fa-dashboard fa-fw"></i> Painel Controle
    </g:link>

</li>

<li>
    <a href="#">
        <i class="fa fa-edit fa-fw"></i> Administração<span class="fa arrow"></span>
    </a>
    <ul class="nav nav-second-level">
        <li>
            <g:link controller="produto" action="index">Produtos</g:link>
        </li>
    </ul>
</li>

<li>
    <a href="#">
        <i class="fa fa-bar-chart-o fa-fw"></i> Vendas<span class="fa arrow"></span>
    </a>
    <ul class="nav nav-second-level">
        <li>
            <g:link controller="pedido" action="index">Pedidos</g:link>
        </li>
    </ul>
</li>


<li>
    <a href="#">
        <i class="fa fa-info-circle fa-fw"></i> Informacional<span class="fa arrow"></span>
    </a>
    <ul class="nav nav-second-level">
        <sec:ifAllGranted roles="ROLE_DESENVOLVEDOR">
            <li>
                <g:link controller="console" action="index" target="_blank">Console</g:link>
            </li>
        </sec:ifAllGranted>

    </ul>
</li>

<li>
    <a href="#">
        <i class="fa fa-balance-scale fa-fw"></i> Logística<span class="fa arrow"></span>
    </a>
    <ul class="nav nav-second-level">
        <li>
            %{--<g:link url="#">Relatórios</g:link>--}%
        </li>
    </ul>
</li>

<li>
    <a href="#">
        <i class="fa fa-money fa-fw"></i> Promocional<span class="fa arrow"></span>
    </a>
    <ul class="nav nav-second-level">
        <li>
            %{--<g:link url="#">Relatórios</g:link>--}%
        </li>
    </ul>
</li>

<li>
    <a href="#">
        <i class="fa fa-user fa-fw"></i> SAC<span class="fa arrow"></span>
    </a>
    <ul class="nav nav-second-level">
        <li>
            %{--<g:link url="#">Relatórios</g:link>--}%
        </li>
    </ul>
</li>