<!DOCTYPE html>
<html lang="pt_BR">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <asset:stylesheet src="application.css"/>
    <asset:javascript src="application.js"/>

    <title><g:layoutTitle default="SIC"/></title>
    <g:layoutHead/>
</head>
<body>

<div id="wrapper">
    <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">SIC - Sistema Integrado de Comércio</a>
        </div>
    </nav>

    <!-- Page Content -->
    <div id="page-wrapper">
        <div class="container">
            <div class="row">
                <hr/>
                <g:layoutBody/>
            </div>
        </div>
    </div>
</div>

<footer class="container-fluid text-center">
    <g:render template="/layouts/footer-geral"/>
</footer>

</body>
</html>