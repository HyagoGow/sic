<!DOCTYPE html>
<html lang="pt_BR">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <asset:stylesheet src="application.css"/>
    <asset:stylesheet src="metisMenu.css"/>
    <asset:javascript src="application.js"/>

    <title><g:layoutTitle default="SIC"/></title>
    <g:layoutHead/>
</head>

<body>

<div id="wrapper">
    <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="${g.createLink(controller: 'dashboard', action: 'redirecionarUsuario')}">
                SIC
            </a>
        </div>
        <!-- /.navbar-header -->

        <ul class="nav navbar-top-links navbar-right">
            <li>
                <a href="#" title="<sec:username/>">
                    <span class="glyphicon glyphicon-user"></span>
                </a>
            </li>
            <li>
                <sec:ifNotSwitched>
                    <g:form controller="logout">
                        <button class="btn btn-link" type="submit" title="Sair"><span class="glyphicon glyphicon-log-out"></span></button>
                    </g:form>
                </sec:ifNotSwitched>
                <sec:ifSwitched>
                    <g:link class="btn btn-danger" controller="logout" action="impersonate" title="Voltar para ${sec.switchedUserOriginalUsername()}">
                        <span class="glyphicon glyphicon-log-out"></span>
                    </g:link>
                </sec:ifSwitched>
            </li>
        </ul>
        <!-- /.navbar-top-links -->

        <div class="navbar-default sidebar" role="navigation">
            <div class="sidebar-nav navbar-collapse">
                <ul class="nav" id="side-menu">
                    <li class="sidebar-search">
                        <div class="input-group custom-search-form">
                            <input type="text" class="form-control" placeholder="Procurar...">
                            <span class="input-group-btn">
                                <button class="btn btn-default" type="button">
                                    <i class="fa fa-search"></i>
                                </button>
                            </span>
                        </div>
                        <!-- /input-group -->
                    </li>

                    <g:render template="/layouts/menu-administrativo"/>
                </ul>
            </div>
        </div>
    </nav>

    <!-- Page Content -->
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="page-header">
                    <h1><g:layoutTitle/></h1>
                </div>
            </div>
            <div class="row">
                <g:layoutBody/>
                <hr>
            </div>
        </div>
    </div>
</div>

<footer class="container-fluid text-center">
    <g:render template="/layouts/footer-geral"/>
</footer>
</body>
</html>