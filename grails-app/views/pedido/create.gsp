<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="layout-administrativo"/>
    <title>Novo Pedido</title>
</head>

<body>
<p>
    <g:link controller="pedido" action="index" class="btn btn-primary">
        <i class="glyphicon glyphicon-list"></i> Pedidos
    </g:link>
</p>

<alert:errorsIfExists/>

<div class="panel panel-primary">
    <div class="panel-heading">
        <h3 class="panel-title">Dados Básicos</h3>
    </div>
    <div class="panel-body">
        <g:form controller="pedido" action="save" method="POST">
            <div class="row">
                <div class="form-group col-md-5">
                    <label for="quantidade">Produto *</label>
                    <g:select name="produto.id" from="${produtos}" value="${pedido?.produto?.id}"
                        class="form-control" optionKey="id"/>
                </div>

                <div class="form-group col-md-5">
                    <label for="quantidade">Cliente *</label>
                    <g:select name="usuario.id" from="${clientes}" value="${pedido?.usuario?.id}"
                              class="form-control" optionValue="username" optionKey="id"/>
                </div>

                <div class="form-group col-md-2">
                    <label for="quantidade">Quantidade *</label>
                    <input type="number" class="form-control" name="quantidade" id="quantidade"
                           value="${pedido?.quantidade}" min="1" step="1" required>
                </div>
            </div>

            <button type="submit" class="btn btn-primary">
                <i class="glyphicon glyphicon-save"></i> Salvar
            </button>
        </g:form>
    </div>
</div>
</body>
</html>