<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="layout-administrativo"/>
    <title>Pedidos</title>
</head>

<body>
<p>
    <g:link controller="pedido" action="create" class="btn btn-primary">
        <i class="glyphicon glyphicon-plus"></i> Novo Pedido
    </g:link>
</p>

<alert:errorsIfExists/>
<alert:successIfExists/>

<g:if test="${pedidoCount > 0}">
    <table class="table table-bordered table-striped">
        <thead>
        <tr>
            <td>Produto</td>
            <td>Quantidade</td>
            <td>Total</td>
        </tr>
        </thead>
        <tbody>
        <g:each in="${pedidoList}" var="pedido">
            <tr>
                <td>${pedido.produto.toString()}</td>
                <td>${pedido.quantidade}</td>
                <td><g:formatNumber number="${pedido.total}" type="currency"/></td>
            </tr>
        </g:each>
        </tbody>
    </table>
    <g:paginate total="${pedidoCount ?: 0}"/>
</g:if>
<g:else>
    <div class="well text-center">
        <strong>SEM REGISTROS</strong>
    </div>
</g:else>
</body>
</html>