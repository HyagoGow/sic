<!doctype html>
<html>
<head>
    <title>Login</title>
    <meta name="layout" content="layout-publico">
</head>

<body>
<div class="col-md-offset-4 col-md-4">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Acesso</h3>
        </div>

        <div class="panel-body">
            <g:if test="${flash.message}">
                <div class="alert alert-danger" role="alert">
                    %{--Bug do spring security. Internationalization não funciona!--}%
                    <g:if test="${flash.message.contains("Sorry, your account has expired.")}">
                        <g:message code="springSecurity.errors.login.expired"/>
                    </g:if>
                    <g:elseif test="${flash.message.contains("Sorry, your password has expired.")}">
                        <g:message code="springSecurity.errors.login.passwordExpired"/>
                    </g:elseif>
                    <g:elseif test="${flash.message.contains("Sorry, your account is disabled")}">
                        <g:message code="springSecurity.errors.login.disabled"/>
                    </g:elseif>
                    <g:elseif test="${flash.message.contains("Sorry, your account is locked.")}">
                        <g:message code="springSecurity.errors.login.locked"/>
                    </g:elseif>
                    <g:elseif test="${flash.message.contains("Sorry, we were not able to find a user with that username and password.")}">
                        <g:message code="springSecurity.errors.login.fail"/>
                    </g:elseif>
                </div>
            </g:if>
            <p>
                <img src="https://dummyimage.com/144/9ca7a8/000000" alt="Adquirência Logo" class="center-block">
            </p>

            <g:form url="${postUrl}" method="POST" name="loginForm" autocomplete="off">
                <div class="form-group input-group">
                    <span class="input-group-addon"><i class="fa fa-envelope-o fa-fw"></i></span>
                    <input class="form-control" type="text" placeholder="Seu email" name="${usernameParameter}"
                           id="${usernameParameter}" autofocus>
                </div>

                <div class="form-group input-group">
                    <span class="input-group-addon"><i class="fa fa-key fa-fw"></i></span>
                    <input class="form-control" placeholder="Sua senha" name="${passwordParameter}"
                           id="${passwordParameter}" type="password">
                </div>

                <hr/>

                <div class="checkbox">
                    <label>
                        <input name="${rememberMeParameter}" id="${rememberMeParameter}"
                               title="Selecione para o sistema lembrar de você!"
                               type="checkbox" ${hasCookie ? 'checked' : ''}>Lembrar de mim
                    </label>
                </div>
                <button class="btn btn-lg btn-success btn-block" type="submit">
                    <i class="fa fa-sign-in" aria-hidden="true"></i> <strong>ENTRAR</strong>
                </button>
            </g:form>
        </div>

        %{--<div class="panel-footer text-center">--}%
        %{--<g:link class="text-left" controller="publico" action="inscricaoCliente">Quero me cadastrar!</g:link>--}%
        %{--|--}%
        %{--<a href="#">Esqueci minha Senha</a>--}%
        %{--</div>--}%
    </div>
</div>

</body>
</html>