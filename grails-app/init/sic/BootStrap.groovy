package sic

class BootStrap {
    def usuarioService
    def produtoService

    def init = { servletContext ->
        usuarioService.verificarAutoriadesBasicas()
        usuarioService.cadastrarClientesTeste()
        produtoService.cadastrarProdutosTeste()
        produtoService.registrarOutputJSON()
    }
    def destroy = {
    }
}