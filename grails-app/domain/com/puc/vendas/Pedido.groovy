package com.puc.vendas

import com.puc.administrativo.Produto
import com.puc.seguranca.Usuario

class Pedido {
    Produto produto
    int quantidade = 1

    static belongsTo = [usuario: Usuario]

    static constraints = {
        produto nullable: false
        quantidade nullable: false, min: 0
    }

    static mapping = {
        id generator: 'increment'
    }

    static transients = ['numero', 'total']

    String getNumero() {
        this.id.toString().padLeft(2, '0')
    }

    Double getTotal(){
        produto.valor * quantidade
    }

    @Override
    String toString() {
        return "Pedido #${numero}"
    }
}