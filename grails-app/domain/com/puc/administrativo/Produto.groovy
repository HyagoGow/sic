package com.puc.administrativo

class Produto {
    String nome
    String codigo
    Double valor

    static constraints = {
        nome nullable: false, blank: false, unique: true
        codigo nullable: true, unique: true
        valor nullable: false, min: 0D
    }

    @Override
    String toString() {
        return "${codigo} - ${nome}"
    }
}