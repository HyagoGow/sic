package com.puc.sistema

import grails.plugin.springsecurity.SpringSecurityUtils

class DashboardController {
    def springSecurityService

    def redirecionarUsuario() {
        if (!springSecurityService.isLoggedIn()) redirect(controller: 'login', action: 'auth')
        else if (SpringSecurityUtils.ifAnyGranted("ROLE_DESENVOLVEDOR, ROLE_ADMINISTRADOR, ROLE_VENDEDOR")) redirect(action: 'administrativo')
        else if (SpringSecurityUtils.ifAnyGranted("ROLE_CLIENTE")) redirect(action: 'cliente')
    }

    def administrativo() {
    }

    def cliente() {
    }
}