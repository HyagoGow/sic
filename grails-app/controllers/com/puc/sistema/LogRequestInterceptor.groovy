package com.puc.sistema


class LogRequestInterceptor {
    def springSecurityService


    boolean before() {
        final String USERNAME = springSecurityService.isLoggedIn() ? springSecurityService.authentication.name :  "ANONIMO"
        final String CLIENTE_ID = request.getHeader("x-forwarded-for") ?: "localhost"
        log.info("${USERNAME}@${CLIENTE_ID}, params: ${params}")
        true
    }

    boolean after() { true }

    void afterView() {
        // no-op
    }
}
