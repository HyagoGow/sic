package com.puc.vendas

import com.puc.administrativo.Produto
import com.puc.extencoes.NotFoundRedirection
import com.puc.extencoes.ValidationsExceptionHandle
import com.puc.seguranca.Usuario

class PedidoController implements ValidationsExceptionHandle, NotFoundRedirection {
    def pedidoService
    def usuarioService
    static allowedMethods = [save: "POST"]

    def index() {
        respond([pedidoList: Pedido.list(), pedidoCount: Pedido.count()])
    }

    def create() {
        List<Produto> produtos = Produto.list()
        List<Usuario> clientes = usuarioService.getClientes()

        if (produtos?.size() == 0) {
            flash.error = "É necessário cadastrar produtos"
            redirect(action: 'index')
            return
        } else if (clientes?.size() == 0) {
            flash.error = "É necessário cadastrar clientes"
            redirect(action: 'index')
            return
        }


        [pedido: new Pedido(params), produtos: produtos, clientes: clientes]
    }

    def save(Pedido pedido) {
        if (pedido == null) {
            notFound()
            return
        }

        pedidoService.salvar(pedido)
        flash.success = "Pedido criado"
        redirect(action: 'index')
    }

    @Override
    String getNomeEntidade() {
        "Pedido"
    }
}