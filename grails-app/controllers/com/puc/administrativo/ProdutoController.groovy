package com.puc.administrativo

import com.puc.extencoes.NotFoundRedirection
import com.puc.extencoes.ValidationsExceptionHandle

class ProdutoController implements ValidationsExceptionHandle, NotFoundRedirection {
    def produtoService
    static allowedMethods = [save: "POST"]

    def index() {
        respond([produtoList: Produto.list(), produtoCount: Produto.count()])
    }

    def create() {
        [produto: new Produto(params)]
    }

    def save(Produto produto) {
        if (produto == null) {
            notFound()
            return
        }

        produtoService.salvar(produto)
        flash.success = "Produto criado"
        redirect(action: 'index')
    }

    @Override
    String getNomeEntidade() {
        "Produto"
    }
}