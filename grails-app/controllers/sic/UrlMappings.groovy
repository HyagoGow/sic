package sic

class UrlMappings {

    static mappings = {
        "/$controller/$action?/$id?(.$format)?"{
            constraints {
                // apply constraints here
            }
        }

        "/"(controller: "dashboard", action: 'redirecionarUsuario')

        "500"(view: '/errors/error')
        "403"(view: '/errors/forbidden')
        "404"(view: '/errors/notFound')
        "405"(view: '/errors/methodNotAllowed')
    }
}