package com.puc.vendas

import com.puc.administrativo.Produto
import grails.converters.JSON
import grails.gorm.transactions.Transactional

@Transactional
class ProdutoService {

    Produto salvar(Produto produto) {
        produto.save()
    }

    void cadastrarProdutosTeste() {
        if (Produto.count() == 0) {
            Produto[] produtos = [
                    new Produto(nome: "Conjunto de Meias 6 peças", codigo: "C00-6", valor: 19.9D),
                    new Produto(nome: "Jogo Marvel Spider Man PS4", codigo: "J00-0", valor: 160D),
                    new Produto(nome: "Garrafa Térmica Transparente", codigo: "C00-12", valor: 9.99D)
            ]

            produtos.each { it.save() }
        }
    }

    def registrarOutputJSON() {
        JSON.registerObjectMarshaller(Produto) { Produto p ->
            [
                    id    : p.id,
                    nome  : p.nome,
                    codigo: p.codigo
            ]
        }
    }
}