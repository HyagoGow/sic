package com.puc.seguranca

import grails.gorm.transactions.Transactional

@Transactional
class UsuarioService {

    final String ROTULO_AUTORIDADE_DESENVOLVEDOR = "ROLE_DESENVOLVEDOR"
    final String ROTULO_AUTORIDADE_ADMINISTRADOR = "ROLE_ADMINISTRADOR"
    final String ROTULO_AUTORIDADE_VENDEDOR = "ROLE_VENDEDOR"
    final String ROTULO_AUTORIDADE_CLIENTE = "ROLE_CLIENTE"

    def verificarAutoriadesBasicas() {
        log.info("Verificando autoridades do sistema...")
        Autoridade desenvolvedor = Autoridade.findOrSaveByAuthority(ROTULO_AUTORIDADE_DESENVOLVEDOR)
        Autoridade administrador = Autoridade.findOrSaveByAuthority(ROTULO_AUTORIDADE_ADMINISTRADOR)
        Autoridade vendedor = Autoridade.findOrSaveByAuthority(ROTULO_AUTORIDADE_VENDEDOR)
        Autoridade cliente = Autoridade.findOrSaveByAuthority(ROTULO_AUTORIDADE_CLIENTE)


        final String usernameDesenvolvedor = "suporte.puc"
        final String passwordDesenvolvedor = "pucminas123"
        log.info("Verificando usuario ${usernameDesenvolvedor}...")
        if (Usuario.findByUsername(usernameDesenvolvedor) == null) {
            if (desenvolvedor.id == null) desenvolvedor.save()
            criar(usernameDesenvolvedor, passwordDesenvolvedor, desenvolvedor)
            log.info("...Usuario ${usernameDesenvolvedor} criado.")
        }
    }

    void cadastrarClientesTeste() {
        if (getClientes()?.size() == 0) {
            Autoridade autoridadeClienete = Autoridade.findByAuthority(ROTULO_AUTORIDADE_CLIENTE)
            criar("cliente.teste.00", "123", autoridadeClienete)
            criar("cliente.teste.01", "123", autoridadeClienete)
            criar("cliente.teste.02", "123", autoridadeClienete)
        }
    }

    /**
     * Cria um usuario usando nome de usuario e senha
     * @param nomeUsuario
     * @param senha
     * @param autoridade
     * @return O usuário criado
     */
    Usuario criar(String nomeUsuario, String senha, Autoridade autoridade) {
        Usuario usuario = new Usuario(username: nomeUsuario, password: senha).save(flush: true)
        UsuarioAutoridade.create(usuario, autoridade)
        return usuario
    }

    /**
     *
     * @return Todos os usuários clientes que estejam ativados
     */
    List<Usuario> getClientes() {
        Autoridade autoridadeClienete = Autoridade.findByAuthority(ROTULO_AUTORIDADE_CLIENTE)
        List<UsuarioAutoridade> usuarioAutoridades = UsuarioAutoridade.findAllByAutoridade(autoridadeClienete)
        List<Usuario> usuarios = []
        usuarioAutoridades.each { if (it.usuario.enabled) usuarios.add(it.usuario) }
        usuarios
    }
}